# Getting start


## Purpose

`RMRun` consists of a set of command line tools to apply R expressions to MR images in nii format. It is designed to be simple and flexible.

## Installation

First install `R` and packages `devtools`, `docopt`, `dplyr` and `miet`:
```R
install.packages('devtools');
install.packages('docopt');
install.packages('dplyr');
devtools::install_git("https://gitlab.inria.fr/miet/miet.git")
```

Then clone this repository (i.e. `git clone https://gitlab.inria.fr/bcombes/RMRun.git` in your console in the target folder) and test the `signalCombination.R` command by using:
```bash
signalMaskFromConditions.R data/01/value.nii.gz --exp 'data1!=0' -o res.nii.gz
```

# List of commands

## `signalCombinaison.R`:

Build a new volume whose each voxel value is computed from a combinaison of values of the corresponding voxel of other volumes given in argument. The input volumes are internally called data1, data2, ... and these names can be used in the combination expression.

Expl:

* Compute mtr according to the basic formula `100*(mt1-mt0/mt1)`:
```bash
signalCombination.R mt1.nii.gz mt0.nii.gz --exp '100*(data1-data2)/data1' -o mtr.nii.gz
```

* Make the volume of zero mean:
```bash
signalCombination.R t1.nii.gz --exp 'data1-mean(as.vector(data1))' -o t1_normed.nii.gz
```

* Compute a norm from 3 volumes:
```bash
signalCombination.R v1.nii.gz v2.nii.gz v3.nii.gz --exp 'sqrt(data1^2+data2^2+data3^2)' -o norm.nii.gz
```


Simpler rule for QC: 
```bash
signalCombinaison.R data/01/value.nii.gz data/01/value.nii.gz --exp '(data1-data1)' -o test.nii.gz ; signalOp.R test.nii.gz --exp 'max(data1)' ## should be 0
```

---

## `signalMaskFromConditions.R`: 

Creates a mask where only voxel satifying a condition are set to 1 and other to 0. The condition can be computed through combinaison of values of the corresponding voxel of other volumes given in argument. The input volumes are internally called data1, data2, ... and these names can be used in the combination expression.

Expl:

* To create a mask from the zero of a volume:
```bash
signalMaskFromConditions.R t2FullResol.nii.gz --exp 'which(data1==0)' -o res.nii.gz
```

* To create a mask from a complex condition:
```bash
signalMaskFromConditions.R v1.nii.gz v2.nii.gz v3.nii.gz --exp 'Reduce(intersect,list(which(data1==0),which(data2<600),which(data3==1))' -o res.nii.gz
```

* To create mask=1 if data1>data2:
```bash
signalMaskFromConditions.R v1.nii.gz v2.nii.gz --exp 'which(data1>data2)' -o res.nii.gz
```


Simpler rule for QC: 
```bash
signalMaskFromConditions.R data/01/vLabel.nii.gz data/01/vLabel.nii.gz --exp 'which(data1==data1)' -o test.nii.gz ; signalOp.R test.nii.gz --exp 'min(data1)' ## should be 1
```

---
 
## `signalReplaceIf.R`:

Replaces the voxel values satisfying a condition given in argument by fixed a value given in argument. Contrary to `signalMaskFromConditions` and `signalCombinaison`, `signalReplaceIf.R` takes as input:
1. an input volume for which we want to replace voxel value.
2. a list of volumes that will be internally called data1,data2, ... and can be used to build the condition.

The input volume can be also be used to build the condition statement but for that it has to be also passed as an element of argument 2. 

* Bound voxel values to 10:
```bash
signalReplaceIf.R data.nii.gz data.nii.gz --exp 'data1>10' -o res.nii.gz --val 10
```

* Mask a volume:
```bash
 signalReplaceIf.R  data.nii.gz mask.nii.gz --exp 'which(data1==0)' --val 0 -o dataCleaned.nii.gz
```

* Merge labels of a mask:
```bash
 signalReplaceIf.R  maskLabel.nii.gz maskLabel.nii.gz --exp 'which(maskLabel>0)' --val 0 -o maskBinary.nii.gz
```


Simpler rule for QC: 
```bash
signalReplaceIf.R data/01/vLabel.nii.gz  data/01/vLabel.nii.gz --exp 'which(data1<1)' --val 1 -o test.nii.gz ; signalOp.R test.nii.gz --exp 'min(data1)' ## should be 1
```

---


## `signalOp.R`:

Apply an operator R^n -> R to a whole nii volume. Result can be stacked in a csv file (option `-o`) and additional fields can be put in the csv file (option `--field`). 

Examples:

* Get the minimal positive value of a given volume.
```bash
signalOp.R vol.nii.gz --exp 'min(data1[which(data1>0)])'
```

* Get the mean of value.nii.gz over voxels of label>0 and for which value>0
```bash
signalOp.R data/01/value.nii.gz data/01/label.nii.gz --exp 'mean(data1[intersect(which(data1>0),which(data2>0))])'  -o test.csv
```

* For complex conditions, creating first a mask can be helpful. For example the last statement can be written as:
```bash
signalMaskFromConditions.R  data/01/value.nii.gz data/01/label.nii.gz --exp 'intersect(which(data1>0),which(data2>0))' -o maskConditions.nii.gz
signalOp.R data/01/value.nii.gz maskConditions.nii.gz --exp 'mean(data1[data2!=0])' -o test.csv
```


Simpler rule for QC: 
```bash
signalOp.R data/01/vLabel.nii.gz data/01/vLabel.nii.gz --exp 'max(data1-data2)' ## should be 0
```


# Todos
i) make signalReplaceIf simpler (only one nii arguments as for others.)
ii) make the interval file loading more generic (external functions + verbose mode :"load 'doedoe.nii.gz as d1'")
iii) change data1 for v1
iv) Finish to deal with documentation/design of the following commands:

## `signalVWOp.R`:

## signalCreate.R

## signalMaskFromLocation.R    

## signalFromLocation.R  
